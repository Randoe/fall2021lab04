import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SkiponacciSequenceTests {
    
    @Test
    public void firstNumberTest() {
        SkiponacciSequence ss = new SkiponacciSequence(2, 4, 1);
        assertEquals(2, ss.getTerm(1));
    } 

    @Test
    public void secondNumberTest() {
        SkiponacciSequence ss = new SkiponacciSequence(2, 4, 1);
        assertEquals(4, ss.getTerm(2));

    }

    @Test
    public void thirdNumberTest() {
        SkiponacciSequence ss = new SkiponacciSequence(2, 4, 1);
        assertEquals(1, ss.getTerm(3));

    }

    @Test
    public void SkipponacciTest() {
        SkiponacciSequence ss = new SkiponacciSequence(2, 4, 1);
        assertEquals(6, ss.getTerm(4));
    }

    @Test
    public void SkipponacciTestTwice() {
        SkiponacciSequence ss = new SkiponacciSequence(2, 4, 1);
        assertEquals(6, ss.getTerm(4));
        assertEquals(6, ss.getTerm(4));
    }


}
