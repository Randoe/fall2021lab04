import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SubtractonacciSequenceTests {
    @Test
    public void firstNumberTest(){
        SubtractonacciSequence f = new SubtractonacciSequence(2, 3);
        int expected = 2;
        assertEquals(expected, f.getTerm(1));
    }
    @Test 
    public void secondNumberTest(){
        SubtractonacciSequence f = new SubtractonacciSequence(2, 3);
        int expected = 3;
        assertEquals(expected, f.getTerm(2));
    }
    @Test
    public void thirdNumberTest(){
        SubtractonacciSequence f = new SubtractonacciSequence(2, 4);
        int expected = -2;
        assertEquals(expected, f.getTerm(3));
    }
    @Test
    public void farNumberTest(){
        SubtractonacciSequence f = new SubtractonacciSequence(2, 4);
        int expected = -22;
        assertEquals(expected, f.getTerm(7));
    }
}
