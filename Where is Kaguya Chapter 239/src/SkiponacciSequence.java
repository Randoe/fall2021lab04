public class SkiponacciSequence extends Sequence{
    private int first;
    private int second;
    private int third;
    
    public SkiponacciSequence(int first, int second, int third){
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public int getTerm(int n){
        int nthNum = 0;
        int firstNum = this.first;
        int secondNum = this.second;
        int thirdNum = this.third;
        if(n == 1){
            return firstNum;
        }
        else if(n == 2){
            return secondNum;
        }
        else if(n == 3){
            return thirdNum;
        }
        for(int i=3; i<n; i++){
            nthNum = firstNum+secondNum;
            firstNum = secondNum;
            secondNum = thirdNum;
            thirdNum = nthNum;
        }
        return nthNum;
    }
}
