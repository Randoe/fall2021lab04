import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class FibonacciSequenceTests {
    @Test
    public void firstNumberTest(){
        FibonacciSequence f = new FibonacciSequence(2, 3);
        int expected = 2;
        assertEquals(expected, f.getTerm(1));
    }
    @Test 
    public void secondNumberTest(){
        FibonacciSequence f = new FibonacciSequence(2, 3);
        int expected = 3;
        assertEquals(expected, f.getTerm(2));
    }
    @Test
    public void thirdNumberTest(){
        FibonacciSequence f = new FibonacciSequence(2, 4);
        int expected = 6;
        assertEquals(expected, f.getTerm(3));
    }
    @Test
    public void farNumberTest(){
        FibonacciSequence f = new FibonacciSequence(2, 4);
        int expected = 42;
        assertEquals(expected, f.getTerm(7));
    }

    @Test
    public void anotherTest() {
        FibonacciSequence f = new FibonacciSequence(1, 1);
        int expected = 3;
        assertEquals(expected, f.getTerm(4));
        assertEquals(expected, f.getTerm(4));
    }
}
