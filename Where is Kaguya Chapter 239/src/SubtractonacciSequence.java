public class SubtractonacciSequence extends Sequence{

    private int firstNumber;
    private int secondNumber;

    public SubtractonacciSequence(int firstNumber, int secondNumber) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    public int getTerm(int n) {
        int firstSequence = 0;
        int firstNumber = this.firstNumber;
        int secondNumber = this.secondNumber;

        if (n == 1) {
            return firstNumber;
        }

        if (n == 2) {
            return secondNumber;
        }

        for (int i = 2; i < n; i++)
        {
            firstSequence = firstNumber - secondNumber;
            firstNumber = secondNumber;
            secondNumber = firstSequence;
        }
        return firstSequence;
    } 
}
