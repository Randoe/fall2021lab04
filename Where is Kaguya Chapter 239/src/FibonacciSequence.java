public class FibonacciSequence extends Sequence {
    private int firstNumber;
    private int secondNumber;

    public FibonacciSequence(int firstNumber, int secondNumber) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    public int getTerm(int n) {
        int firstSequence = 0;
        int UpdatedFirstNumber = this.firstNumber;
        int UpdatedsecondNumber = this.secondNumber;

        if (n == 1) {
            return UpdatedFirstNumber;
        }

        if (n == 2) {
            return UpdatedsecondNumber;
        }

        for (int i = 2; i < n; i++) {
            firstSequence = UpdatedFirstNumber + UpdatedsecondNumber;
            UpdatedFirstNumber = UpdatedsecondNumber;
            UpdatedsecondNumber = firstSequence;
        }
        return firstSequence;
    } 
}
