public class SequenceApplication {
    public static void main(String[] args){

        FibonacciSequence fb = new FibonacciSequence(2, 4);
        //testing parse() and print(0)
        String testString = "Fib;1;1;x;Skip;2;2;3;Sub;4;2;x;Fib;2;3;x";
        print(parse(testString), 4); 

        //testing validate()
        boolean testBool = validate(testString);
        System.out.println(testBool);
    }
    // sequence method
    public static Sequence[] parse(String input){
        String[] splits = input.split(";");
        Sequence[] arraySequences = new Sequence[splits.length/4];
        for(int i = 0; i<splits.length; i=i+4){
            if(splits[i].equals("Fib")){
                arraySequences[i] = new FibonacciSequence(Integer.parseInt(splits[i+1]), Integer.parseInt(splits[i+2]));
                }
            else if(splits[i].equals("Sub")){
                arraySequences[i] = new SubtractonacciSequence(Integer.parseInt(splits[i+1]), Integer.parseInt(splits[i+2]));
            }
            else if(splits[i].equals("Skip")){
                arraySequences[i] = new SkiponacciSequence(Integer.parseInt(splits[i+1]), Integer.parseInt(splits[i+2]), Integer.parseInt(splits[i+3]));
            }
        }
        return arraySequences;
    }
     
    // print method
    public static void print(Sequence[] sequences, int n)
    {
        String buildUp = "";

        for(int i = 0; i < sequences.length; i++)
        {
            buildUp += "Sequence Number " + i + ": ";
            for(int j = 0; j < n; j++)
            {
                if(j == n-1)
                {
                    buildUp += sequences[i].getTerm(j + 1) + "\n";
                }
                else
                {
                    buildUp += sequences[i].getTerm(j + 1) + ", ";
                }
            }
        }
        System.out.println(buildUp);
    }


    // validate method
    public static boolean validate(String str)  
    {
        String[] a = str.split(";");

        boolean validated = true;
        while(validated)
        {
            if (a.length % 4 != 0) 
            {
                validated = false;
            }
            for(int i =0; i < a.length; i++)
            {
                if((i+1)%4 == 0)
                {
                    if(!a[i].equals("Fib") && !a[i].equals("Skip") && !a[i].equals("Sub"))
                    {
                        validated = false;
                    }
                    if(a[i].equals("Fib") || a[i].equals("Sub"))
                    {
                        try
                        {
                            Integer.parseInt(a[i+1]);
                            Integer.parseInt(a[i+2]);
                        }
                        catch(NumberFormatException e)
                        {
                            validated = false;
                        }
                        if(!a[i+3].equals("x"))
                        {
                            validated = false;
                        }

                    }

                     if(a[i].equals("Skip"))
                    {
                        try
                        {
                            Integer.parseInt(a[i+1]);
                            Integer.parseInt(a[i+2]);
                            Integer.parseInt(a[i+3]);
                        }
                        catch(NumberFormatException e)
                        {
                            validated = false;
                        }
                    }
                }
            }

        }
        return validated;
    }

}